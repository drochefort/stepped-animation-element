# Stepped Animation Web Component

This web component encapsulates the paced animation functionality required for MX RTI Habitat-based content. It may be used on both Interaction or Authoring screens.


## Guidelines to Animators

- Animations are created with Adobe Animate CC
- The beginning of each step must have a label on the main timeline
- Animations are exported to HTML5 canvas. The name of the project must match the name of the exported javascript file.
- Image assets, if any, should be provided along.

## Assumptions

- The animation name matches the Javascript animation file name. This is Animate CC's default behaviour.
- All animations are checked into Habitat under `trunk/assets/animations/{animationName}/{animationName}.js`.

## Development

```shell
npm install
npm start
```

## Tests

```shell
npm install
bower install
npm test
```

## API Documentation

### Markup

```html
<stepped-animation animation-name="myAnimationName">
</stepped-animation>
```

### Configurable attributes

- **animation-name** :*string* the name of the animation file, and the name of the animation function.
- **animation-path** :*string* the relative path where all animations are located (default:  `../../animations`)
- **step** :*number* initial step number (default: `0`)

### Read-only attributes

- **ended** :*boolean* this attribute is set when animation has ended
- **is-ready** :*boolean* this attribute is set when animation is ready
- **paused** :*boolean* this attribute is set when animation is paused
- **total-steps** :*number* the total number of steps in the animation
- **status** :*string* shows elements status. `ok` means element is working properly, `error` means there was a problem loading the animation, see console for details.

### Advanced attributes

- **playback-rate** :*number* this factor can be used to increase/decrease the animation velocity  (default: `1`)


### Events

- **animation-ready**: fires when animation is ready to play
- **animation-resized**: fires every time animation canvas gets resized
- **animation-step-end**: fires whenever playback stops at the end of a step. 
- **animation-end**: fires when the end of the animation is reached and playback pauses. 

Event details `evt.detail` carries the following values:

- **step** :*number* the current step
- **totalSteps** :*number* the total number of steps

The **animation-resized** event details also carries the canvas new *width* and *height*.
