(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1024,
	height: 768,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib.balloon = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3.1,0,0,4).p("ABdppIi6AAIDGA+IjSgZIgQAnQgRAxgIA1QgZCpBOCMQAkBBAqA7QAlA1AHANQAPAeAAAlQAAAkgOBQQgTBvg6CKQgaA6gFASQgLAhAJAR");
	this.shape.setTransform(72,204.8,0.49,0.49,0,0,0,-2.6,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999999").s().p("ACvCHQgngZgfgJQgogLgCAGIgCALQgDAIgJAGQgXAOgQACQgNACgigFQgVgDgMgSQgKgPgTADQgfAMgPADQgXAFgfgUQgSgLgBgHQgBgFAMgJQAzgmAZggIAng1QgHAIAAgHIAAhUQAfANCUgUIAGAOQAGAVAAAlQAAAlAbASQAOAKAWAGQALAEAMAcQAOAhAAAfQAAAYATAIIAIAEQABADgJAGQgFAFgHAAQgKAAgOgKg");
	this.shape_1.setTransform(73.5,179.2,0.49,0.49);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(7.3,1,1).p("AW8AAQAAllhzlIQhwk8jLj0QjLj0kIiGQkRiLkqAAQkpAAkRCLQkICGjLD0QjLD0hwE8QhzFIAAFlQAAFmBzFIQBwE8DLD0QDLD0EICGQEQCLEqAAQEqAAERiLQEIiGDLj0QDLj0Bwk8QBzlIAAlmg");
	this.shape_2.setTransform(72,86.4,0.49,0.49);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CCCCCC").s().p("Ao6ZYQkIiGjLj0QjLj0hwk8QhzlIAAlmQAAllBzlIQBwk8DLj0QDLj0EIiGQERiLEpAAQEqAAERCLQEICGDLD0QDLD0BwE8QBzFIAAFlQAAFmhzFIQhwE8jLD0QjLD0kICGQkRCLkqAAQkqAAkQiLg");
	this.shape_3.setTransform(72,86.4,0.49,0.49);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


(lib.baloonAnimYellow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.balloon();
	this.instance.parent = this;
	this.instance.setTransform(40,66.7,1,1,0,0,0,40,66.7);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 0, 1, 34, 21, 0, 0)];
	this.instance.cache(-6,-6,155,244);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:39.7,regY:66.5,scaleX:1.04,scaleY:1.04,rotation:5.5,x:36.7,y:63.7},15).to({regX:39.6,scaleX:0.93,scaleY:0.93,rotation:-3.9,x:37.3,y:54.3},14).to({regX:40,regY:66.7,scaleX:1,scaleY:1,rotation:0,x:40,y:66.7},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


(lib.baloonAnimRed = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.balloon();
	this.instance.parent = this;
	this.instance.setTransform(40,66.7,1,1,0,0,0,40,66.7);
	this.instance.filters = [new cjs.ColorFilter(1, 0.109375, 0.1484375, 1, 0, 0, 0, 0)];
	this.instance.cache(-6,-6,155,244);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:39.7,regY:66.5,scaleX:1.04,scaleY:1.04,rotation:5.5,x:36.7,y:63.7},15).to({regX:39.6,scaleX:0.93,scaleY:0.93,rotation:-3.9,x:37.3,y:54.3},14).to({regX:40,regY:66.7,scaleX:1,scaleY:1,rotation:0,x:40,y:66.7},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


(lib.baloonAnimPurple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.balloon();
	this.instance.parent = this;
	this.instance.setTransform(40,66.7,1,1,0,0,0,40,66.7);
	this.instance.filters = [new cjs.ColorFilter(1, 0.51953125, 0.8203125, 1, 0, 0, 0, 0)];
	this.instance.cache(-6,-6,155,244);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:39.7,regY:66.5,scaleX:1.04,scaleY:1.04,rotation:5.5,x:36.7,y:63.7},15).to({regX:39.6,scaleX:0.93,scaleY:0.93,rotation:-3.9,x:37.3,y:54.3},14).to({regX:40,regY:66.7,scaleX:1,scaleY:1,rotation:0,x:40,y:66.7},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


(lib.baloonAnimGreen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.balloon();
	this.instance.parent = this;
	this.instance.setTransform(40,66.7,1,1,0,0,0,40,66.7);
	this.instance.filters = [new cjs.ColorFilter(0.51953125, 1, 0.359375, 1, 0, 0, 0, 0)];
	this.instance.cache(-6,-6,155,244);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:39.7,regY:66.5,scaleX:1.04,scaleY:1.04,rotation:5.5,x:36.7,y:63.7},15).to({regX:39.6,scaleX:0.93,scaleY:0.93,rotation:-3.9,x:37.3,y:54.3},14).to({regX:40,regY:66.7,scaleX:1,scaleY:1,rotation:0,x:40,y:66.7},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


(lib.baloonAnimBlue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.balloon();
	this.instance.parent = this;
	this.instance.setTransform(40,66.7,1,1,0,0,0,40,66.7);
	this.instance.filters = [new cjs.ColorFilter(0.03125, 0.73828125, 1, 1, 0, 0, 0, 0)];
	this.instance.cache(-6,-6,155,244);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:39.7,regY:66.5,scaleX:1.04,scaleY:1.04,rotation:5.5,x:36.7,y:63.7},15).to({regX:39.6,scaleX:0.93,scaleY:0.93,rotation:-3.9,x:37.3,y:54.3},14).to({regX:40,regY:66.7,scaleX:1,scaleY:1,rotation:0,x:40,y:66.7},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-3.6,151.1,240.3);


// stage content:



(lib.balloons = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{ballons1:0,ballons2:25,ballons3:50,ballons4:75,ballons5:100,ballons6:126,ballons7:152,ballons8:175,ballons9:200,end1:225,end2:250});

	// other
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(7.3,1,1).p("EgptgMBMBTbAAAIAAYDMhTbAAAg");
	this.shape.setTransform(510.2,155.9,1.88,1.88);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,51,0.388)").s().p("Egh5AMbIAA41MBDzAAAIAAY1g");
	this.shape_1.setTransform(414.5,502.8,1.811,1.88);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},225).to({state:[{t:this.shape},{t:this.shape_1}]},25).wait(25));

	// ballons
	this.instance = new lib.baloonAnimRed();
	this.instance.parent = this;
	this.instance.setTransform(62.8,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_1 = new lib.baloonAnimBlue();
	this.instance_1.parent = this;
	this.instance_1.setTransform(259.6,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_2 = new lib.baloonAnimGreen();
	this.instance_2.parent = this;
	this.instance_2.setTransform(470.3,126.7,1.034,1.034,0,0,0,40.1,66.8);

	this.instance_3 = new lib.baloonAnimPurple();
	this.instance_3.parent = this;
	this.instance_3.setTransform(679.1,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_4 = new lib.baloonAnimYellow();
	this.instance_4.parent = this;
	this.instance_4.setTransform(904.9,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_5 = new lib.baloonAnimPurple();
	this.instance_5.parent = this;
	this.instance_5.setTransform(679.1,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_6 = new lib.baloonAnimYellow();
	this.instance_6.parent = this;
	this.instance_6.setTransform(904.9,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_7 = new lib.baloonAnimBlue();
	this.instance_7.parent = this;
	this.instance_7.setTransform(259.6,126.7,1.034,1.034,0,0,0,40,66.8);

	this.instance_8 = new lib.baloonAnimGreen();
	this.instance_8.parent = this;
	this.instance_8.setTransform(470.3,126.7,1.034,1.034,0,0,0,40.1,66.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}}]},25).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}}]},25).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}},{t:this.instance_3,p:{x:679.1,y:126.7}}]},25).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}},{t:this.instance_3,p:{x:679.1,y:126.7}},{t:this.instance_4,p:{x:904.9,y:126.7}}]},25).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}},{t:this.instance_5},{t:this.instance_4,p:{x:904.9,y:126.7}},{t:this.instance_3,p:{x:62.8,y:463.3}}]},26).to({state:[{t:this.instance},{t:this.instance_1,p:{regX:40,x:259.6,y:126.7}},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}},{t:this.instance_5},{t:this.instance_6},{t:this.instance_3,p:{x:62.8,y:463.3}},{t:this.instance_4,p:{x:259.6,y:463.4}}]},26).to({state:[{t:this.instance},{t:this.instance_7},{t:this.instance_2,p:{regX:40.1,x:470.3,y:126.7}},{t:this.instance_5},{t:this.instance_6},{t:this.instance_3,p:{x:62.8,y:463.3}},{t:this.instance_4,p:{x:259.6,y:463.4}},{t:this.instance_1,p:{regX:40.1,x:470.3,y:463.4}}]},23).to({state:[{t:this.instance},{t:this.instance_7},{t:this.instance_8},{t:this.instance_5},{t:this.instance_6},{t:this.instance_3,p:{x:62.8,y:463.3}},{t:this.instance_4,p:{x:259.6,y:463.4}},{t:this.instance_1,p:{regX:40.1,x:470.3,y:463.4}},{t:this.instance_2,p:{regX:40,x:679.1,y:463.4}}]},25).to({state:[{t:this.instance},{t:this.instance_7},{t:this.instance_8},{t:this.instance_5},{t:this.instance_6},{t:this.instance_3,p:{x:62.8,y:463.3}},{t:this.instance_4,p:{x:259.6,y:463.4}},{t:this.instance_1,p:{regX:40.1,x:470.3,y:463.4}},{t:this.instance_2,p:{regX:40,x:679.1,y:463.4}}]},25).to({state:[{t:this.instance},{t:this.instance_7},{t:this.instance_8},{t:this.instance_5},{t:this.instance_6},{t:this.instance_3,p:{x:62.8,y:463.3}},{t:this.instance_4,p:{x:259.6,y:463.4}},{t:this.instance_1,p:{regX:40.1,x:470.3,y:463.4}},{t:this.instance_2,p:{regX:40,x:679.1,y:463.4}}]},25).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(529.7,437.9,156.2,248.4);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;