(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1024,
	height: 768,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/numberCards_atlas_.png?1478893884460", id:"numberCards_atlas_"}
	]
};



lib.ssMetadata = [
		{name:"numberCards_atlas_", frames: [[0,204,123,140],[0,0,952,202]]}
];


// symbols:



(lib.Bitmap2 = function() {
	this.spriteSheet = ss["numberCards_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap5 = function() {
	this.spriteSheet = ss["numberCards_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.rightHand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.parent = this;
	this.instance.setTransform(123,0,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,123,140);


(lib.Numbers = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,952,202);


(lib.leftHand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AirKPQgDgCAKhEQAKhDgCgDIhpiHIhpiHQgDgCgUiRQgUiPgCgDQgDgCDGhfQDFhggCgCQgEgDAEhWIAPlBQgGgGASAHIAVAJQgDgCBVAdQBOAcAFAAQgCAHARDRIAUDbIAAAAIAAAAIA3AeIA3AfQgBAFAmCnQAoCygCgDQgDgCg8BzQg8BzgCgDQgDgCAFBaQAFBagCgDQgDgCjmACIirACQg6AAgBgCg");
	mask.setTransform(52.3,63);

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.parent = this;

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(11.1,0,82.5,128.7);


// stage content:
(lib.numberCards = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{intro:0,step1:63,step2:94,step3:154,step4:214});

	// numbers
	this.instance = new lib.Numbers();
	this.instance.parent = this;
	this.instance.setTransform(476,-115,1,1,0,0,0,476,101);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:205},16,cjs.Ease.get(0.7)).wait(259));

	// leftHand
	this.instance_1 = new lib.leftHand();
	this.instance_1.parent = this;
	this.instance_1.setTransform(147.5,602,1,1,0,0,0,61.5,70);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(40).to({_off:false},0).to({y:362},22,cjs.Ease.get(0.84)).wait(32).to({x:247.5},30,cjs.Ease.get(0.36)).wait(31).to({regX:52.3,regY:64.4,x:243.3,y:356.4},0).wait(1).to({x:248.2},0).wait(1).to({x:253},0).wait(1).to({x:257.7},0).wait(1).to({x:262.4},0).wait(1).to({x:266.9},0).wait(1).to({x:271.4},0).wait(1).to({x:275.8},0).wait(1).to({x:280.2},0).wait(1).to({x:284.4},0).wait(1).to({x:288.6},0).wait(1).to({x:292.7},0).wait(1).to({x:296.7},0).wait(1).to({x:300.6},0).wait(1).to({x:304.4},0).wait(1).to({x:308.2},0).wait(1).to({x:311.9},0).wait(1).to({x:315.5},0).wait(1).to({x:319},0).wait(1).to({x:322.4},0).wait(1).to({x:325.8},0).wait(1).to({x:329},0).wait(1).to({x:332.2},0).wait(1).to({x:335.3},0).wait(1).to({x:338.4},0).wait(1).to({x:341.3},0).wait(1).to({x:344.2},0).wait(1).to({x:347},0).wait(1).to({x:349.7},0).wait(1).to({regX:61.5,regY:70,x:361.5,y:362},0).wait(30).to({x:429.5},30,cjs.Ease.get(0.3)).wait(31));

	// rightHand
	this.instance_2 = new lib.rightHand();
	this.instance_2.parent = this;
	this.instance_2.setTransform(775.5,602,1,1,0,0,0,61.5,70);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(37).to({_off:false},0).to({y:362},22,cjs.Ease.get(0.86)).wait(35).to({x:665.5},30,cjs.Ease.get(0.36)).wait(31).to({x:660.9},0).wait(1).to({x:656.3},0).wait(1).to({x:651.8},0).wait(1).to({x:647.4},0).wait(1).to({x:643.1},0).wait(1).to({x:638.9},0).wait(1).to({x:634.7},0).wait(1).to({x:630.6},0).wait(1).to({x:626.6},0).wait(1).to({x:622.6},0).wait(1).to({x:618.8},0).wait(1).to({x:615},0).wait(1).to({x:611.2},0).wait(1).to({x:607.6},0).wait(1).to({x:604},0).wait(1).to({x:600.5},0).wait(1).to({x:597.1},0).wait(1).to({x:593.8},0).wait(1).to({x:590.5},0).wait(1).to({x:587.3},0).wait(1).to({x:584.2},0).wait(1).to({x:581.1},0).wait(1).to({x:578.2},0).wait(1).to({x:575.3},0).wait(1).to({x:572.5},0).wait(1).to({x:569.7},0).wait(1).to({x:567},0).wait(1).to({x:564.5},0).wait(1).to({x:561.9},0).wait(1).to({x:559.5},0).wait(30).to({x:487.5},30,cjs.Ease.get(0.3)).wait(31));

	// text
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AieEOQg8gzAAhRQAAg0AegpQAegqA3gWIAAgDQgygbgYgfQgZggABgvQAAhGA5guQA6guBWgBQBcAAA4AtQA2AsAABFQAAApgaAqQgaAngyAXIAAADQA6AXAeAkQAfAlAAA4QAABQg+A1Qg/A1heAAQhiAAg8g0gAhvA1QgXAiAAAtQAAA3AnAlQAmAlA6AAQA8AAAkgfQAkggAAg2QAAgmgQgXQgRgXgxgYQgUgIgXgJIg7gUQgmATgWAjgAhUjrQgiAYAAAqQAAAfARAVQARAWAiARQAPAHAdANQAbALAcAJQAogcAQgeQAQgdAAgmQAAgtgigaQgjgag0gBQgzAAghAag");
	this.shape.setTransform(684.9,622.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Aj1B4IAAhDIHrAAIAABDgAj1g1IAAhCIHrAAIAABCg");
	this.shape_1.setTransform(593.2,626.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AimE2IAAg/ICBAAIAAmdIiBAAIAAg4QAaAAAegEQAegFAPgJQATgJAMgRQAKgPACgbIA+AAIAAIrIB/AAIAAA/g");
	this.shape_2.setTransform(502.7,622.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AghEDIAAjiIjiAAIAAhBIDiAAIAAjiIBDAAIAADiIDiAAIAABBIjiAAIAADig");
	this.shape_3.setTransform(409.8,626.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AibE1IEnogIleAAIAAhIIGlAAIAABcIkWIMg");
	this.shape_4.setTransform(318.6,622.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AjQE7IAAhXIBYhLQAsglAlgkQBOhOAegrQAdguAAg1QAAgvgfgcQghgbg2AAQgnAAgrANQgsANgqAcIgEAAIAAhYQAdgOAygMQAxgMAtAAQBeAAA2AuQA2AuAABPQAAAigJAfQgKAegQAcQgRAXgVAZQgXAageAeQgtAsguApIhYBLIFLAAIAABHg");
	this.shape_5.setTransform(502.1,621.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhNEzQglgPgdgbQgkgjgTg5QgTg5AAhQQAAhQARg/QAShAAmgxQAlgvA6gaQA3gbBNAAQAZAAAQACQARACARAGIAABPIgEAAQgMgGgXgGQgYgFgYAAQhYAAg1A4Qg1A3gIBfQAigVAigLQAigLAqAAQAoAAAeAIQAeAHAgAWQAkAZATAkQASAnAAA3QAABfg+A7Qg+A8haAAQgsAAglgOgAhGgJQgeAJggAPIgBASIgBAVQAABCAOAmQANAmAYAWQATASAXAJQAWAIAYAAQA8AAAiglQAjgkAAhFQAAgngLgZQgMgagagSQgTgMgXgEQgYgEgXAAQgjAAgfAIg");
	this.shape_6.setTransform(318.6,622.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ah5E2QgxgNgggOIAAhYIAGAAQAjAYAwAPQAwAPAsAAQAZAAAdgIQAdgJATgRQASgSAKgWQAJgWAAgiQAAghgKgWQgLgVgSgNQgTgNgbgEQgbgGgcAAIgmAAIAAhDIAdAAQA+AAAmgZQAlgbAAgzQAAgWgJgQQgKgSgRgKQgSgMgUgDQgVgEgYgBQgnAAgtAPQgsAOgnAZIgFAAIAAhWQAegPAxgMQAxgNAvAAQArAAAiAJQAjAIAbASQAfAVAOAcQAQAcAAAmQAAAzgkAnQgkAmgyAKIAAAFQAUAEAaALQAZAKATAOQATASANAaQANAbAAArQAAArgPAjQgQAkgcAaQgeAcgqAOQgrAOgwABQg0AAgygMg");
	this.shape_7.setTransform(501.8,622.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ah4EwQgvgKghgPIAAhYIAGAAQAiAWAuAPQAuAPAsAAQAbAAAcgIQAbgJAWgVQASgRAKgZQAJgZAAghQAAgfgLgXQgLgVgUgOQgVgPgfgFQgdgGgmgBQglAAgiAGQgiAEgYAEIAAk9IFyAAIAABIIkiAAIAAClIAlgDIAggBQAwABAmAIQAmAJAfAWQAhAWATAiQASAkAAA2QAAAsgQAnQgQAngbAcQgeAdgpAQQgpAPg1AAQgyAAgvgLg");
	this.shape_8.setTransform(318.9,623.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AA+E1IAAivIkmAAIAAhfIEplaIBMAAIAAF3IBcAAIAABCIhcAAIAACvgAiwBEIDuAAIAAkUg");
	this.shape_9.setTransform(501,622.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AA+E1IAAivIkmAAIAAhfIEplaIBMAAIAAF3IBcAAIAABCIhcAAIAACvgAiwBEIDuAAIAAkUg");
	this.shape_10.setTransform(317.6,622.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},63).to({state:[]},30).to({state:[{t:this.shape_6},{t:this.shape_3},{t:this.shape_5},{t:this.shape_1},{t:this.shape}]},31).to({state:[]},30).to({state:[{t:this.shape_8},{t:this.shape_3},{t:this.shape_7},{t:this.shape_1},{t:this.shape}]},31).to({state:[]},29).to({state:[{t:this.shape_10},{t:this.shape_3},{t:this.shape_9},{t:this.shape_1},{t:this.shape}]},30).wait(31));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(512,168,952,202);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;