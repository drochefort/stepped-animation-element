(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



// stage content:
(lib.triangle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{intro:0,edge1:24,edge2:50,edge3:74,introCorner:116,corner1:139,corner2:164,corner3:190});

	// edges
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag1BmIAAgVIAqAAIAAiGIgqAAIAAgTIASgBQAKgCAFgDQAGgDAEgFQADgGABgJIATAAIAAC2IApAAIAAAVg");
	this.shape.setTransform(177.3,164.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(7.3,1,1).p("ApcQpMAS5ghR");
	this.shape_1.setTransform(215.5,192.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhEBnIAAgcIAegZIAagYQAYgaAKgMQAKgPAAgSQAAgQgKgIQgLgJgRAAQgMAAgPADQgOAFgOAJIgBAAIAAgdQAKgEAQgFQAQgDAPAAQAdAAASAPQASAPAAAZQgBANgCAKQgDAJgGAKQgFAGgIAIQgHAJgKAKIgcAbIgdAZIBsAAIAAAXg");
	this.shape_2.setTransform(382.2,168.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag1BmIAAgVIAqAAIAAiGIgqAAIAAgTIASgBQAKgCAFgDQAGgDAEgFQADgGABgJIATAAIAAC2IApAAIAAAVg");
	this.shape_3.setTransform(177.3,164.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(7.3,1,1).p("AzNQpMAS7ghRMATgAhH");
	this.shape_4.setTransform(278,192.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgnBmQgQgFgLgEIAAgcIACAAQAMAHAPAFQARAFAOAAQAHAAAKgDQAJgCAGgGQAGgGAEgIQADgHAAgLQgBgLgDgHQgDgHgHgEQgGgEgIgCQgJgBgIgBIgNAAIAAgVIAKAAQASABANgJQAMgJAAgQQAAgIgDgFQgDgGgGgEQgFgDgHgBIgNgBQgNgBgPAFQgPAEgMAJIgCAAIAAgdQAKgEAQgEQAQgEAPAAQANAAALACQAMADAJAGQAKAHAFAJQAFAJAAANQAAARgMANQgMAMgQADIAAACIAPAFQAJADAFADQAHAHAEAIQAEAJAAAOQABAOgGAMQgEAMgKAIQgKAKgOAEQgOAFgOgBQgRAAgRgDg");
	this.shape_5.setTransform(270.1,334.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhDBnIAAgcIAdgZIAagYQAZgaAJgMQAKgPAAgSQAAgQgKgIQgLgJgRAAQgNAAgOADQgOAFgOAJIgCAAIAAgdQALgEAPgFQAQgDAQAAQAdAAASAPQASAPgBAZQAAANgCAKQgDAJgGAKQgFAGgIAIQgGAJgLAKIgcAbIgdAZIBrAAIAAAXg");
	this.shape_6.setTransform(384.2,186.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(7.3,1,1).p("AzSQuMAmlAAAAzIQkMAS7ghRMATgAhH");
	this.shape_7.setTransform(277.5,192.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag2BmIAAgVIArAAIAAiGIgrAAIAAgTIATgBQAKgCAFgDQAGgDAEgFQADgGABgJIATAAIAAC2IAqAAIAAAVg");
	this.shape_8.setTransform(276.3,65.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AkPDvIAcgyIA2hkICPkBIAnhGIACACIAmBEICYEBIA5BkIAeAyg");
	this.shape_9.setTransform(276.9,107.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhDBnIAAgcIAcgZIAbgYQAZgaAJgNQAKgPAAgRQAAgQgLgIQgKgKgRABQgNAAgOADQgPAFgNAJIgBAAIAAgcQAJgGAQgDQARgFAPAAQAdAAASAQQARAPAAAZQAAAMgCALQgDAKgGAJQgFAGgHAIQgIAJgKAKIgcAbIgdAZIBrAAIAAAXg");
	this.shape_10.setTransform(423.2,300.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("ADZQ4IAbgyIA3hkICPkEIAnhGIACACIAoBEICXEEIA6BkIAeAygAr5pXIAcgyIA3hkICPkEIAmhGIACACIAoBEICYEEIA6BkIAdAyg");
	this.shape_11.setTransform(325.8,192);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgmBmQgRgFgKgEIAAgdIABAAQAMAIAPAFQAQAFAPAAQAHAAAKgDQAJgDAGgFQAGgHADgHQAEgHAAgLQAAgLgEgHQgDgHgHgEQgGgFgIgBQgJgBgJgBIgMAAIAAgUIAKAAQASAAANgJQAMgIAAgRQAAgIgCgFQgEgGgFgEQgHgDgGgBIgNgBQgNgBgPAFQgOAFgNAIIgCAAIAAgcQAKgFAQgEQARgEAPgBQAMAAAMADQALADAJAGQAKAGAFAKQAFAJAAANQAAAQgLAOQgMAMgRAEIAAABIAPAFQAJADAFADQAHAHAEAIQAEAJABAOQAAAOgGAMQgEALgKAJQgKAJgOAFQgOAFgOAAQgSAAgPgEg");
	this.shape_12.setTransform(134,300.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AKoQ4IAbgyIA3hkICPkEIAnhGIACACIAoBEICXEEIA6BkIAeAygAzIQ4IAcgyIA3hkICPkEIAmhGIACACIAoBEICYEEIA6BkIAdAygAkqpXIAcgyIA3hkICPkEIAmhGIACACIAmBEICYEEIA6BkIAdAyg");
	this.shape_13.setTransform(279.5,192);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1},{t:this.shape,p:{x:177.3,y:164.4}}]},24).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape,p:{x:177.3,y:164.4}},{t:this.shape_2}]},26).to({state:[{t:this.shape_7},{t:this.shape,p:{x:168.3,y:186.4}},{t:this.shape_6},{t:this.shape_5}]},24).to({state:[]},41).to({state:[{t:this.shape_9},{t:this.shape_8}]},24).to({state:[{t:this.shape_11},{t:this.shape_8},{t:this.shape_10}]},25).to({state:[{t:this.shape_13},{t:this.shape_8},{t:this.shape_10},{t:this.shape_12}]},26).wait(25));

	// Layer 7
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AzIQ4MASAggpIAmhGIACACIAmBEMATDAgpg");
	this.shape_14.setTransform(279.5,192);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AzIQ4IOe6PIAcgyIA3hkICPkEIAmhGIACACIAmBEICYEEIA6BkIAdAyIPUaPg");
	this.shape_15.setTransform(279.5,192);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14}]}).to({state:[{t:this.shape_14}]},24).to({state:[{t:this.shape_15}]},25).wait(166));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(432,284,245.1,216.1);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;