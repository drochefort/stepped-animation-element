(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1024,
	height: 768,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/Bitmap2.jpg?1478792101890", id:"Bitmap2"},
		{src:"images/Bitmap3.jpg?1478792101890", id:"Bitmap3"},
		{src:"images/Bitmap4.jpg?1478792101890", id:"Bitmap4"},
		{src:"images/Bitmap5.jpg?1478792101890", id:"Bitmap5"},
		{src:"images/Bitmap6.jpg?1478792101890", id:"Bitmap6"}
	]
};



lib.ssMetadata = [];


// symbols:



(lib.Bitmap2 = function() {
	this.initialize(img.Bitmap2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,294,312);


(lib.Bitmap3 = function() {
	this.initialize(img.Bitmap3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,346,492);


(lib.Bitmap4 = function() {
	this.initialize(img.Bitmap4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,483,449);


(lib.Bitmap5 = function() {
	this.initialize(img.Bitmap5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,354,354);


(lib.Bitmap6 = function() {
	this.initialize(img.Bitmap6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,422,386);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap5();
	this.instance.parent = this;
	this.instance.setTransform(-147.9,-147.9,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147.9,-147.9,295.9,295.9);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap5();
	this.instance.parent = this;
	this.instance.setTransform(-145.2,-150.4,0.836,0.836,1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150.4,-150.4,301,301);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap5();
	this.instance.parent = this;
	this.instance.setTransform(-147.9,-147.9,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147.9,-147.9,295.9,295.9);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap4();
	this.instance.parent = this;
	this.instance.setTransform(-129,-119.9,0.534,0.534);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-119.9,258,239.9);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap4();
	this.instance.parent = this;
	this.instance.setTransform(-129,-119.9,0.534,0.534);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-119.9,258,239.9);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap4();
	this.instance.parent = this;
	this.instance.setTransform(-129,-119.9,0.534,0.534);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-119.9,258,239.9);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap4();
	this.instance.parent = this;
	this.instance.setTransform(-129,-119.9,0.534,0.534);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-119.9,258,239.9);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.parent = this;
	this.instance.setTransform(-122.8,-130.3,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122.8,-130.3,245.7,260.8);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.parent = this;
	this.instance.setTransform(-122.8,-130.3,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122.8,-130.3,245.7,260.8);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.parent = this;
	this.instance.setTransform(-122.8,-130.3,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122.8,-130.3,245.7,260.8);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap3();
	this.instance.parent = this;
	this.instance.setTransform(-144.5,-205.5,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-144.5,-205.5,289.2,411.2);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap6();
	this.instance.parent = this;
	this.instance.setTransform(-176.3,-161.3,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-176.3,-161.3,352.7,322.6);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap6();
	this.instance.parent = this;
	this.instance.setTransform(-176.3,-161.3,0.836,0.836);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-176.3,-161.3,352.7,322.6);


// stage content:
(lib.triangleImages = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{start:0});

	// Layer 5
	this.instance = new lib.Tween14("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(843.9,166.9);

	this.instance_1 = new lib.Tween15("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(843.9,166.9,0.621,0.621,-176,0,0,-0.1,-0.1);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween16("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(843.9,166.9,1,1,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},11).to({state:[{t:this.instance_2}]},8).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,regX:-0.1,regY:-0.1,scaleX:0.62,scaleY:0.62,rotation:-176},11).wait(9));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:false},11).to({_off:true,regX:0,regY:0,scaleX:1,scaleY:1,rotation:-353},8).wait(1));

	// Layer 4
	this.instance_3 = new lib.Tween10("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(188,130.9);

	this.instance_4 = new lib.Tween11("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(152,119.9);
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween12("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(160,119.9);
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween13("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(188,130.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3}]}).to({state:[{t:this.instance_4}]},4).to({state:[{t:this.instance_4}]},4).to({state:[{t:this.instance_5}]},5).to({state:[{t:this.instance_6}]},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({_off:true,x:152,y:119.9},4).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off:false},4).to({x:168,y:124.8},4).to({_off:true,x:160,y:119.9},5).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off:false},5).to({_off:true,x:188,y:130.9},6).wait(1));

	// Layer 3
	this.instance_7 = new lib.Tween7("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(156.9,608.4);

	this.instance_8 = new lib.Tween8("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(160.9,546.1,1.298,1.298,26,0,0,0.1,0.1);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween9("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(156.9,608.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},9).to({state:[{t:this.instance_9}]},10).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({_off:true,regX:0.1,regY:0.1,scaleX:1.3,scaleY:1.3,rotation:26,x:160.9,y:546.1},9).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({_off:false},9).to({_off:true,regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,x:156.9,y:608.4},10).wait(1));

	// Layer 2
	this.instance_10 = new lib.Tween1("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(795.3,562.3);

	this.instance_11 = new lib.Tween2("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(719.3,554.3,1,1,35);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({_off:true,rotation:35,x:719.3,y:554.3},5).wait(8).to({_off:false,rotation:0,x:795.3,y:562.3},6).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({_off:false},5).to({scaleX:1,scaleY:1,rotation:-36.9,x:835.3,y:533.4},8).to({_off:true,scaleX:1,scaleY:1,rotation:0,x:795.3,y:562.3},6).wait(1));

	// Layer 1
	this.instance_12 = new lib.Tween3("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(474.6,342.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({scaleX:1.39,scaleY:1.39},5).to({scaleX:1,scaleY:1},2).to({scaleX:1.23,scaleY:1.23},4).to({scaleX:1.14,scaleY:1.14},2).to({scaleX:1,scaleY:1},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(546,395,957.9,727.8);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;