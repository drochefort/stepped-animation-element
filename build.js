const shell = require('shelljs');
const { rollup } = require('rollup');
const babel = require('rollup-plugin-babel');
const fs = require('fs');
const Vulcanize = require('vulcanize');

const elementFolder = 'src/stepped-animation';
const buildFolder = 'build/stepped-animation';
const tempFolder = 'temp/stepped-animation';

function transpile() {
    return rollup({
        entry: `${elementFolder}/js/stepped-animation.js`,
        plugins: [babel()],
        dest: 'bundle.js',
    }).then(bundle => {
        bundle.write({
            format: 'iife',
            dest: `${buildFolder}/js/stepped-animation.js`,
        });

        return bundle;
    }).then(bundle =>
        bundle.write({
            format: 'iife',
            dest: `${tempFolder}/js/stepped-animation.js`,
        }));
}

function copy() {
    shell.rm('-rf', 'temp');
    shell.rm('-rf', 'build');
    shell.mkdir('-p', `${tempFolder}/css`);
    shell.mkdir('-p', `${tempFolder}/js`);
    shell.cp(`${elementFolder}/stepped-animation.html`, `${tempFolder}/stepped-animation.html`);
    shell.cp(`${elementFolder}/css/stepped-animation.css`, `${tempFolder}/css/stepped-animation.css`);
    shell.mkdir('-p', `${buildFolder}/css`);
    shell.mkdir('-p', `${buildFolder}/js`);
    shell.cp(`${elementFolder}/css/stepped-animation.css`, `${buildFolder}/css/stepped-animation.css`);

    return Promise.resolve();
}

function vulcanize() {
    const vulcan = new Vulcanize({
        inlineScripts: true,
        inlineCss: true,
    });

    return vulcan.process(`${tempFolder}/stepped-animation.html`, (err, html) => {
        if (err) {
            throw err;
        }

        fs.writeFileSync(`${buildFolder}/stepped-animation.html`, html);
    });
}

copy()
    .then(transpile)
    .then(vulcanize)
    .catch(e => console.error(e));