// require the module as normal
const browserSync = require('browser-sync');

// Start the server
browserSync({
    server: ['src/','.'],
    files: [
        'src/**/*.js',
        'src/**/*.css',
        'src/**/*.html',
    ],
});