/* global createjs, Polymer, HTMLElement, lib, ss, images */

/**
 * In IE, Firefox and Chrome typeof HTMLElement is "function" so it works fine,
 * but in Safari it is object, so we need to use this polyfill to make it work in safari
 */
if (typeof HTMLElement !== 'function') {
    const htmlElement = () => { };
    htmlElement.prototype = HTMLElement.prototype;
    /*eslint-disable */
    HTMLElement = htmlElement;
    /*eslint-enable */
}

class SteppedAnimation extends HTMLElement {

    static get is() {
        return 'stepped-animation';
    }

    /**
     * This method acts like the constructor in Polymer context
     */
    beforeRegister() {
        this.properties = {
            animationName: {
                type: String,
                value: '',
                reflectToAttribute: true,
                notify: true,
            },
            animationPath: {
                type: String,
                value: '../../animations',
                reflectToAttribute: true,
                notify: true,
            },
            step: {
                type: Number,
                value: 0,
                reflectToAttribute: true,
                notify: true,
                observer: 'onStepChanged',
            },
            totalSteps: {
                type: Number,
                value: 0,
                reflectToAttribute: true,
            },
            isReady: {
                type: Boolean,
                value: false,
                reflectToAttribute: true,
                notify: true,
                observer: 'onReadyChanged',
            },
            paused: {
                type: Boolean,
                value: true,
                reflectToAttribute: true,
                notify: true,
                observer: 'onPausedChanged',
            },
            ended: {
                type: Boolean,
                value: false,
                reflectToAttribute: true,
                notify: true,
                observer: 'onEndedChanged',
            },
            status: {
                type: String,
                value: 'ok',
                reflectToAttribute: true,
            },
            playbackRate: {
                type: Number,
                value: 1,
            },
        };

        this.observers = [
            'animationChanged(animationPath, animationName)',
        ];
    }

    reset() {
        this.step = 0;
    }

    next() {
        if (this.step < this.totalSteps - 1) {
            ++this.step;
        }
    }

    previous() {
        if (this.step > 0) {
            --this.step;
        }
    }

    play() {
        this.onStepChanged();
    }

    pause(ended = false) {
        this.animation.gotoAndStop(this.timeline.position);
        this.paused = true;
        this.ended = ended;
    }

    handleTick(evt) {
        if (this.timeline) {

            const endOfTimeline = this.timeline.position >= this.timeline.duration - 1;
            const endOfStep = this.timeline.position >= this.stopPoint - 1;

            if (endOfTimeline || endOfStep) {
                this.pause(endOfTimeline);
            }

        }

        this.stage.update(evt);
    }

    animationChanged(animationPath, animationName) {
        console.log('ANIMATION CHANGED in stepped-animation', animationPath, animationName);
        if (animationPath && animationName) {
            // Inject animation script into the local DOM
            // <script on-load="animationLoaded" on-error="animationError"></script>
            const script = document.createElement('script');
            script.addEventListener('load', () => this.animationLoaded());
            script.addEventListener('error', () => this.animationError());
            script.setAttribute('src', `${animationPath}/${animationName}/${animationName}.js`);
            Polymer.dom(this.root).appendChild(script);
        }
    }

    animationLoaded() {
        lib.properties.manifest.forEach(asset => {
            asset.src = `${this.animationPath}/${this.animationName}/${asset.src}`;
        });

        console.log('animationLoaded', lib.properties.manifest);
        //This function is always called, irrespective of the content. You can use the constiable "stage" after it is created in token create_stage.
        this.canvas = this.$$('canvas');

        if (lib.properties.manifest.length > 0) {
            const loader = new createjs.LoadQueue(false);
            loader.addEventListener('fileload', (evt) => this.handleFileLoad(evt));
            loader.addEventListener('complete', (evt) => this.handleComplete(evt));
            loader.loadManifest(lib.properties.manifest);
        } else {
            this.handleComplete();
        }
    }

    animationError() {
        console.error(`animation not found ${this.animationPath}/${this.animationName}/${this.animationName}.js`);
        this.status = 'error';
    }

    handleComplete(evt) {
        if (evt) {
            const queue = evt.target;
            const ssMetadata = lib.ssMetadata;

            ssMetadata.forEach(asset => {
                console.log('asset', asset);
                ss[asset.name] = new createjs.SpriteSheet({
                    images: [queue.getResult(asset.name)],
                    frames: asset.frames,
                });
            });
        }

        this.animation = new lib[this.animationName]();

        console.log('LIB PROPERTIES', lib.properties);

        this.stage = new createjs.Stage(this.canvas);
        this.timeline = this.animation.timeline;

        const labels = this.timeline._labels;
        this.cuePoints = Object.keys(labels)
            .map(label => labels[label])
            .sort((a, b) => a - b);

        if (!this.cuePoints.includes(0)) {
            this.cuePoints.unshift(0);
        }

        this.totalSteps = this.cuePoints.length;

        console.log('CUE POINTS', this.cuePoints, this.timeline.duration);

        //This function is always called, irrespective of the content. You can use the constiable 'stage' after it is created in token create_stage.
        this.stage.addChild(this.animation);

        //Registers the 'tick' event listener.
        createjs.Ticker.setFPS(this.playbackRate * lib.properties.fps);
        createjs.Ticker.addEventListener('tick', (evt) => this.handleTick(evt));

        this.isReady = true;
        this.play();
        //Code to support hidpi screens and responsive scaling.
        this.initResizer(true, 'both', true, 1);
    }

    handleFileLoad(evt) {
        console.log('handleFileLoad', evt);
        if (evt.item.type == 'image') { images[evt.item.id] = evt.result; }
    }

    onStepChanged() {
        if (!this.isReady) {
            return;
        }

        this.paused = false;
        this.stopPoint = this.cuePoints[this.step + 1] || this.timeline.duration;
        this.animation.gotoAndPlay(this.cuePoints[this.step]);
    }

    onPausedChanged() {
        if (this.isReady && this.paused) {
            this.fire('animation-step-end', {
                step: this.step,
                totalSteps: this.totalSteps,
            });
        }
    }

    onEndedChanged() {
        console.log('onEndedChanged', this.isReady, this.ended);
        if (this.isReady && this.ended) {
            this.fire('animation-end', {
                step: this.step,
                totalSteps: this.totalSteps,
            });
        }
    }

    onReadyChanged() {
        if (this.isReady) {
            this.fire('animation-ready', {
                step: this.step,
                totalSteps: this.totalSteps,
            });
        }
    }

    getParentWidth() {
        const parentNode = this.parentNode;
        const width = parentNode ? parentNode.clientWidth : window.innerWidth;
        console.log('ANIMATION WIDTH', parentNode, width);
        return width;
    }

    getParentHeight() {
        const parentNode = this.parentNode;
        const parentHeight = parentNode ? parentNode.clientHeight : window.innerHeight;

        const pRatio = window.devicePixelRatio || 1;
        const ratio = lib.properties.height / lib.properties.width;
        console.log('ANIMATION HEIGHT', parentHeight, ratio + this.getParentWidth());

        return Math.max(parentHeight, pRatio * ratio + this.getParentWidth());
    }


    // @private
    initResizer(isResp, respDim, isScale, scaleType) {
        this.lastS = 1;
        window.addEventListener('resize', () => this.resizeCanvas(isResp, respDim, isScale, scaleType));
        this.resizeCanvas(isResp, respDim, isScale, scaleType);
    }
    // @private
    resizeCanvas(isResp, respDim, isScale, scaleType) {
        const w = lib.properties.width;
        const h = lib.properties.height;
        const iw = this.getParentWidth();
        const ih = this.getParentHeight();
        const pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h;

        let sRatio = 1;
        if (isResp) {
            if ((respDim == 'width' && this.lastW == iw) || (respDim == 'height' && this.lastH == ih)) {
                sRatio = this.lastS;
            }
            else if (!isScale) {
                if (iw < w || ih < h)
                    sRatio = Math.min(xRatio, yRatio);
            }
            else if (scaleType == 1) {
                sRatio = Math.min(xRatio, yRatio);
            }
            else if (scaleType == 2) {
                sRatio = Math.max(xRatio, yRatio);
            }
        }
        this.canvas.width = w * pRatio * sRatio;
        this.canvas.height = h * pRatio * sRatio;
        this.canvas.style.width = w * sRatio + 'px';
        this.canvas.style.height = h * sRatio + 'px';
        this.stage.scaleX = pRatio * sRatio;
        this.stage.scaleY = pRatio * sRatio;
        this.lastW = iw; this.lastH = ih; this.lastS = sRatio;

        this.fire('animation-resized', {
            step: this.step,
            totalSteps: this.totalSteps,
            width: this.canvas.width,
            height: this.canvas.height,
        });
    }
}

Polymer(SteppedAnimation);