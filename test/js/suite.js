const EVENTS = {
    READY: 'animation-ready',
    STEP_END: 'animation-step-end',
    END: 'animation-end',
    RESIZE: 'animation-resized',
};

let totalSteps;

describe('default setup', () => {
    beforeEach(() => fixture('default'));

    it('should get ready on first step by default', done => {
        const el = document.querySelector('stepped-animation');
        el.addEventListener(EVENTS.READY, (evt) => {
            totalSteps = evt.detail.totalSteps;
            expect(evt.detail.step).to.equal(0);
            expect(evt.detail.totalSteps).to.equal(totalSteps);
            done();
        });

    });
});

describe('steps', () => {
    beforeEach(() => fixture('steps'));

    it('should set step attribute', done => {
        const el = document.querySelector('stepped-animation');

        el.addEventListener(EVENTS.READY, (evt) => {
            totalSteps = evt.detail.totalSteps;
            expect(evt.detail.step).to.equal(2);
            el.setAttribute('step', 4);
        });

        el.addEventListener(EVENTS.STEP_END, (evt) => {
            expect(evt.detail.step).to.equal(4);
            done();
        });
    });

    it('should reset animation', done => {
        const el = document.querySelector('stepped-animation');

        el.addEventListener(EVENTS.READY, (evt) => {
            totalSteps = evt.detail.totalSteps;
            expect(evt.detail.step).to.equal(2);
            el.reset();
        });

        el.addEventListener(EVENTS.STEP_END, (evt) => {
            expect(evt.detail.step).to.equal(0);
            done();
        });
    });

    it('should play animation forward step by step', function (done) {
        this.timeout(80000);
        const el = document.querySelector('stepped-animation');

        el.addEventListener(EVENTS.READY, (evt) => {
            totalSteps = evt.detail.totalSteps;
            expect(evt.detail.step).to.equal(2);
            el.reset();
        });

        el.addEventListener(EVENTS.STEP_END, () => el.next());

        el.addEventListener(EVENTS.END, (evt) => {
            expect(evt.detail.step).to.equal(totalSteps - 1);
            done();
        });
    });

    it('should play animation backward step by step', function (done) {
        this.timeout(80000);
        const el = document.querySelector('stepped-animation');

        el.addEventListener(EVENTS.READY, (evt) => {
            totalSteps = evt.detail.totalSteps;
            expect(evt.detail.step).to.equal(2);
            el.setAttribute('step', 7);
        });

        el.addEventListener(EVENTS.STEP_END, (evt) => {
            if (evt.detail.step === 0) {
                done();
            } else {
                el.previous();
            }
        });

    });
});

describe('a11y', () => {
    it('is accessible', () => a11ySuite('default'));
});